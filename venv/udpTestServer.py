import asyncio
from utils import set_ctrl_msg
from utils import check_msg_type
from AVPFrame import control_message_types as control_message_types
from AVPFrame import ctr_msg_response_pairs
# from AVPFrame import avp_types as avp_types
from L2TPFrame import L2TPFrame
from AVPFrame import AVPFrame

class EchoServerProtocol(asyncio.DatagramProtocol):
    def __init__(self):
        self.msg_list = []
        self.await_loop_size = 0
        self.ctr_msg_name = ""
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):    # Funkcja odpowiedzialna za obsługę otrzymanych pakietów, wywoływana automatycznie po odebraniu każdego pakietu
        print('Received %r from %s' % (data, addr))
        rec_frame = L2TPFrame(data)
        if rec_frame.type_flag == 1:
            first_frame = AVPFrame(0,data)
            if first_frame.atr_type == 0:
                self.ctr_msg_type = first_frame.atr_val[0]
                self.await_loop_size,self.ctr_msg_name = check_msg_type(self.ctr_msg_type,control_message_types)
            self.msg_list.append(data)
            if len(self.msg_list) == self.await_loop_size:
                self.msg_list.clear()
                self.await_loop_size = 0
                if self.ctr_msg_name not in ctr_msg_response_pairs:
                    zlb_msg = L2TPFrame(1,0,0,0,0,2,20,20,"0").pack_header()
                    self.transport.sendto(zlb_msg,addr)
                    print ("Send %r to %s " % (zlb_msg,addr))
                else:
                    response_msg_name = ctr_msg_response_pairs[self.ctr_msg_name]
                    response_msg = set_ctrl_msg(response_msg_name,"LNS","1","1",1,1,0,0,0,2,20,20)
                    for msg in response_msg:
                        self.transport.sendto(msg,addr)
                        print ("Send %r to %s " % (msg,addr))
        elif rec_frame.type_flag == 0:
            payload = rec_frame.get_payload()
            print ("Message received: " + payload)
            response_frame = L2TPFrame(0,0,0,0,0,2,20,20,"Thank you for your message! Have a nice day!")
            self.transport.sendto(response_frame.pack_header(),addr)
            print ("Send %r to %s " % (response_frame.pack_header(),addr))

async def main():
    print("Starting UDP server")
    loop = asyncio.get_running_loop()
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: EchoServerProtocol(),
        local_addr=('127.0.0.1', 9999))

    try:
        await asyncio.sleep(60)
    finally:
        transport.close()

asyncio.run(main())
