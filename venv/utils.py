import AVPFrame

def check_msg_type(id,msg_dict):
    items = msg_dict.items()
    for item in items:
        if item[1][0] == id:
            if type(item[1][1]) == str:
                return 1,item[0]
            else:
                return len(item[1][1]),item[0]

#Funkcja pomocnicza do przygotowywania całych wiadomości kontrolnych
#Jedna wiadomość kontrola składa się z kilku ramek typu AVP
def set_ctrl_msg(ctrl_type,host_name,assigned_tunnel_id,assigned_session_id,*args): 
    avp_types = AVPFrame.AVPTypes(ctrl_type,host_name,assigned_tunnel_id,assigned_session_id)
    frames = []
    if ctrl_type == "SCCCN":
        frames.append(getattr(avp_types,"get_avp_"+AVPFrame.control_message_types["SCCCN"][1])(*args).pack_header())
        return frames
    else:
        for entry in AVPFrame.control_message_types[ctrl_type][1]:
            # getattr() wywołuje odpowiednie metody z obiektu klasy AVPTypes. To które metody zostaną wywołane zależy od tego jakie AVP składają się na konkretną wiadomość kontrolną 
            frames.append(getattr(avp_types,"get_avp_"+entry)(*args).pack_header())
        return frames
