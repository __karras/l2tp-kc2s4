import subprocess

import kivy
kivy.require('1.0.6') # replace with your current kivy version !

from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.widget import Widget
from kivy.properties import Property
from subprocess import Popen, PIPE
import time
import threading
from kivy.properties import ObjectProperty

polaczenie= 0
class MyGrid(Widget):
    #def __init__(self, **kwargs):
     #   super(MyGrid,self).__init__(**kwargs)
    def polacz(self):
        global polaczenie

        if polaczenie==1:
            self.ids.lac_output.text ="Połączenie jest już ustanowione"
            self.ids.lns_output.text ="Połączenie jest już ustanowione"
        else:
            self.ids.lac_output.text= "Wysłano komunikat SCCRQ \n" \
                                  "Odebrano komunikat SCCRP \n" \
                                  "Wysłano komunikat SCCCN \n" \
                                    "Wysłano komunikat ICRQ \n" \
                                      "Odebrano komunikat ICRP \n" \
                                      "Wysłano komunikat ICCN "
            self.ids.lns_output.text="Osebrano komnikat SCCRQ \n" \
                                 "Wysłano komnikat SCCRP \n" \
                                 "Osebrano komnikat SCCCN \n" \
                                     "Odebrano komunikat ICRQ \n" \
                                     "Wysłano komunikat ICRP \n" \
                                     "Odebrano komunikat ICCN"
        polaczenie=1
        self.ids.stan_polaczenia.text="Klient jest połączony z serwerem"

    def zamknij_program(self):
        MyApp.get_running_app().stop()
    def zerwij_polaczenie(self):
        global polaczenie
        if polaczenie==1:
            self.ids.lac_output.text= "Wysłano komunikat CDN \n" \
                                  "Odebrano komunikat StopCCN \n" \
                                  "Wysłano komunikat ZLB \n" \

            self.ids.lns_output.text="Odebrano komnikat CDN \n" \
                                 "Wysłano komnikat StopCCN \n" \
                                 "Odebrano komnikat ZLB \n"
            self.ids.stan_polaczenia.text = "Klient jest rozłączony z serwerem"

        else:
            self.ids.lac_output.text = "Nie ma tunelu i sesji do zamknięcia"
            self.ids.lns_output.text = "Nie ma tunelu i sesji do zamknięcia"
        polaczenie=0
    def reset(self):
        global polaczenie
        polaczenie=0
        self.ids.lac_output.text = ""

        self.ids.lns_output.text = ""
        self.ids.stan_polaczenia.text="Klient jest rozłączony z serwerem"


    def wyslij_wiadomosc(self):
        wiadomosc= ObjectProperty(None)
        if polaczenie==1:
            self.ids.lns_output.text =self.wiadomosc.text
            self.ids.lac_output.text = "Wysłano ramkę L2TP z wiadomośćią: " + self.wiadomosc.text
            self.ids.lns_output.text = "Odebrano ramkę L2TP z wiadomośćią: " + self.wiadomosc.text
        else:
            self.ids.lac_output.text = "Nie można wysłac wiadomości bez nawiązanego połączenia."
            self.ids.lns_output.text = "Nie można wysłac wiadomości bez nawiązanego połączenia."

class MyApp(App):

    def build(self):
        return MyGrid()


if __name__ == '__main__':
   # serwer=Popen("udpTestServer.py", shell="True", stdout=PIPE, stderr=PIPE)
    #stdout, stderr = serwer.communicate()

    MyApp().run()
