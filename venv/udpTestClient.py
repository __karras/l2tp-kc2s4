import asyncio
import sys
from utils import set_ctrl_msg
from utils import check_msg_type
from L2TPFrame import L2TPFrame
from AVPFrame import AVPFrame
from AVPFrame import control_message_types
#from AVPFrame import ctr_msg_response_pairs

dest_addr='127.0.0.1'
dest_port=9999

class EchoClientProtocol(asyncio.DatagramProtocol):
    def __init__(self, message, on_con_lost):
        self.message = message
        self.on_con_lost = on_con_lost
        self.transport = None
        self.msg_recv_counter = 0
        self.await_loop_size = 0
        self.ctr_msg_name = ""

    def connection_made(self, transport): #Funkcja do wysyłania pakietów do serwera
        self.transport = transport
        if type(self.message) != list:
            print('Send:', self.message)
            self.transport.sendto(self.message)
        else:
            for msg in self.message:
                print('Send:', msg)
                self.transport.sendto(msg)

    def datagram_received(self, data, addr): #Funkcja do obsługi odebranych pakietów
        print ("Received:",data)
        rec_frame = L2TPFrame(data)
        if rec_frame.type_flag == 1:
            if rec_frame.length_flag == 0:
                self.transport.close()
                return
            first_frame = AVPFrame(0,data)
            if first_frame.atr_type == 0:
                self.ctr_msg_type = first_frame.atr_val[0]
                self.await_loop_size,self.ctr_msg_name = check_msg_type(self.ctr_msg_type,control_message_types)
            self.msg_recv_counter = self.msg_recv_counter + 1
            if self.msg_recv_counter == self.await_loop_size:
                self.msg_recv_counter = 0
                self.await_loop_size = 0
                # response_msg_name = ctr_msg_response_pairs[self.ctr_msg_name]
                # response_msg = set_ctrl_msg(response_msg_name,"LAC","1","1",1,1,0,0,0,2,20,20)
                # for msg in response_msg:
                #     print('Send:', msg)
                #     self.connection_made(self.transport)
                self.transport.close()
        elif rec_frame.type_flag == 0:
            payload = rec_frame.get_payload()
            print ("Message received: " + payload)
            self.transport.close()


    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self, exc):
        print("Connection closed")
        self.on_con_lost.set_result(True)


async def polacz():
    loop = asyncio.get_running_loop()
    on_con_lost = loop.create_future()
    SCCRQ_msg = set_ctrl_msg("SCCRQ","LAC","1","1",1,1,0,0,0,2,20,20)
    transport, protocol = await loop.create_datagram_endpoint(
        lambda : EchoClientProtocol(SCCRQ_msg,on_con_lost),
        remote_addr=(dest_addr, dest_port))
    try:
        await on_con_lost
    finally:
        transport.close()

    loop = asyncio.get_running_loop()
    on_con_lost = loop.create_future()
    SCCCN_msg = set_ctrl_msg("SCCCN","LAC","1","1",1,1,0,0,0,2,20,20)
    transport, protocol = await loop.create_datagram_endpoint(
        lambda : EchoClientProtocol(SCCCN_msg,on_con_lost),
        remote_addr=(dest_addr, dest_port))
    try:
        await on_con_lost
    finally:
        transport.close()

    loop = asyncio.get_running_loop()
    on_con_lost = loop.create_future()
    ICRQ_msg = set_ctrl_msg("ICRQ","LAC","1","1",1,1,0,0,0,2,20,20)
    transport, protocol = await loop.create_datagram_endpoint(
        lambda : EchoClientProtocol(ICRQ_msg,on_con_lost),
        remote_addr=(dest_addr, dest_port))
    try:
        await on_con_lost
    finally:
        transport.close()

    loop = asyncio.get_running_loop()
    on_con_lost = loop.create_future()
    ICCN_msg = set_ctrl_msg("ICCN","LAC","1","1",1,1,0,0,0,2,20,20)
    transport, protocol = await loop.create_datagram_endpoint(
        lambda : EchoClientProtocol(ICCN_msg,on_con_lost),
        remote_addr=(dest_addr, dest_port))
    try:
        await on_con_lost
    finally:
        transport.close()

    print ("Zestawiono tunel oraz sesję z serwerem LNS")
    # return "Zestawiono tunel oraz sesję z serwerem LNS"

async def wyslij_wiadomosc(user_msg):
    loop = asyncio.get_running_loop()
    on_con_lost = loop.create_future()
    msg = L2TPFrame(0,0,0,0,0,2,20,20,user_msg).pack_header()
    transport, protocol = await loop.create_datagram_endpoint(
        lambda : EchoClientProtocol(msg,on_con_lost),
        remote_addr=(dest_addr, dest_port))
    try:
        await on_con_lost
    finally:
        transport.close()

async def zakoncz_polaczenie():
    loop = asyncio.get_running_loop()
    on_con_lost = loop.create_future()
    StopCCN_msg = set_ctrl_msg("StopCCN","LAC","1","1",1,1,0,0,0,2,20,20)
    transport, protocol = await loop.create_datagram_endpoint(
        lambda : EchoClientProtocol(StopCCN_msg,on_con_lost),
        remote_addr=(dest_addr, dest_port))
    try:
        await on_con_lost
    finally:
        transport.close()
    
    loop = asyncio.get_running_loop()
    on_con_lost = loop.create_future()
    CDN_msg = set_ctrl_msg("CDN","LAC","1","1",1,1,0,0,0,2,20,20)
    transport, protocol = await loop.create_datagram_endpoint(
        lambda : EchoClientProtocol(CDN_msg,on_con_lost),
        remote_addr=(dest_addr, dest_port))
    try:
        await on_con_lost
    finally:
        transport.close()

    print ("Zakończono połączenie z serwerem LNS")
    # return "Zakończono połączenie z serwerem LNS"

# python.exe ../udpTestClient polacz
# python.exe ../udpTestClient wyslij_wiadomosc "jakas tam wiadomosc (byleby w cudzyslowie zeby to program potraktowal jako jeden argument jesli bede spacje"
# python.exe ../udpTestClient zakoncz_polaczenie
async def main():
    if sys.argv[1] == "polacz":
        await polacz()
    elif sys.argv[1] == "wyslij_wiadomosc":
        if len(sys.argv) < 3:
            print ("Nie podałeś wiadomości do przesłania!")
            # return ("Nie podałeś wiadomości do przesłania!")
        else:
            await wyslij_wiadomosc(sys.argv[2])
    elif sys.argv[1] == "zakoncz_polaczenie":
        await zakoncz_polaczenie()

asyncio.run(main())