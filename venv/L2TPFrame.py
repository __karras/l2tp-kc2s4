# Klasa zwykłej ramki L2TP
# Składa się z poszczególnych pól ramki oraz funkcji, która konwertuje wszystko do postaci bitowej
# W konstruktorze dochodzi do sprawdzenia ile zadano argumentów, jeśli jeden (w domyśle ciąg bitów) to funkcja odkodowuje co trzeba i zapisuje do poszczególnych zmiennych.
# Jeśli więcej niż jeden (w domyśle poszczególne wartości pól ramki) to przekazane wartości są przypisywane zmienny żeby później można było wszystko wygodnie zakodować i przesłać
# 
# test = L2TPFrame(1,0,0,0,0,2,20,20,"test")
# 


import bitstruct

l2tp_msg_header_format = bitstruct.compile('b1b1p2b1p1b1b1p4u4u16u16')
l2tp_msg_amount_of_octets = 6
class L2TPFrame:
    def __init__(self,*args):
        if len(args) == 1:
            self.type_flag,self.length_flag,self.seq_flag,self.offset_flag,self.priority_flag,self.version,self.tunnel_id,self.session_id = l2tp_msg_header_format.unpack(args[0][:l2tp_msg_amount_of_octets])
            self.payload =  args[0][l2tp_msg_amount_of_octets:]
        elif len(args) > 1:
            self.type_flag,self.length_flag,self.seq_flag,self.offset_flag,self.priority_flag,self.version,self.tunnel_id,self.session_id,self.payload = args
    def pack_header(self):
        return l2tp_msg_header_format.pack(self.type_flag,self.length_flag,self.seq_flag,self.offset_flag,self.priority_flag,self.version,self.tunnel_id, self.session_id)+bitstruct.pack('t'+str(len(self.payload)*8),self.payload)
    def get_payload(self):
        return bitstruct.unpack('t'+str(len(self.payload)*8),self.payload)[0]
