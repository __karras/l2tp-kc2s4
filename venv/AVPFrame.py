# Klasa ramki wiadomości kontrolnej typu AVP
# Dziedziczy po L2TPFrame, bo współdzielą część pól


import bitstruct
from L2TPFrame import L2TPFrame

avp_header_format = bitstruct.compile('b1b1p4u10u16u16')
avp_header_amount_of_octets = 6
l2tp_msg_header_format = bitstruct.compile('b1b1p2b1p1b1b1p4u4u16u16')

#format: "Name of Control Message type" : "ID of control message" , "List of apvs used in control message"
control_message_types = {
    "SCCRQ" : (1,("message_type","protocol_version","host_name","framing_cap","assigned_tunnel_id")),
    "SCCRP" : (2,("message_type","protocol_version","framing_cap","host_name","assigned_tunnel_id")),
    "SCCCN" : (3,("message_type")),
    "StopCCN" : (4,("message_type","assigned_tunnel_id","result_code")),
    "ICRQ" : (10,("message_type","assigned_session_id","call_serial_number")),
    "ICRP" : (11,("message_type","assigned_session_id")),
    "ICCN" : (12,("message_type","connect_speed","framing_type")),
    "CDN" : (14,("message_type","result_code","assigned_session_id"))
    
}

#format: "ID of avp" : "Name of avp", "Format of avp used in encoding strings as bits"
avp_types = {
    0 : ("message_type","u16"),
    1 : ("result_code","u16"),
    2 : ("protocol_version", "u8u8"),
    3 : ("framing_cap","p30b1b1"),
    7 : ("host_name", "t{length}"),
    9 : ("assigned_tunnel_id","u16"),
    14 : ("assigned_session_id","u16"),
    15 : ("call_serial_number","u32"),
    19 : ("framing_type","p30b1b1"),
    24 : ("connect_speed","f32")
}

ctr_msg_response_pairs = {
    "SCCRQ" : "SCCRP",
    "SCCRP" : "SCCCN",
    "ICRQ" : "ICRP",
    "ICRP" : "ICCN",
}

# Działa na podobnej zasadzie co L2TPFrame + dodatkowe pola wynikajace z AVP
# Ramka AVP jest zbudowowana na ramce L2TP tj. ramka AVP to ramka L2TP z tym, że w niej payload jest jeszcze dzielony dodatkowo na kolejne pola
# Wywoływany jest konstruktor z L2TPFrame, żeby tam zrobić przypisywanie wartości do zmiennych a nie kopiować tutaj drugi raz to samo
class AVPFrame(L2TPFrame):
    def __init__(self,avp_flags,*args):
        if len(args) == 1:
            super().__init__(*args)
            self.m_flag,self.h_flag,self.avp_length,self.vendor_id,self.atr_type = avp_header_format.unpack(self.payload[:avp_header_amount_of_octets])
            self.atr_val = bitstruct.unpack(avp_types[self.atr_type][1].format(length=self.avp_length-6),self.payload[avp_header_amount_of_octets:])
        elif len(args) > 1:
            self.m_flag,self.h_flag,self.avp_length,self.vendor_id,self.atr_type,self.atr_val = avp_flags
            super().__init__(*args,avp_flags)
    def pack_header(self):
        encoded_header = l2tp_msg_header_format.pack(self.type_flag,self.length_flag,self.seq_flag,self.offset_flag,self.priority_flag,self.version,self.tunnel_id, self.session_id) + avp_header_format.pack(self.m_flag,self.h_flag,self.avp_length,self.vendor_id,self.atr_type)+self.atr_val
        return encoded_header

# Klasa przechowująca typy wiadomości AVP
class AVPTypes():
    def __init__(self,ctrl_msg_type,host_name,assigned_tunnel_id,assigned_session_id):
        self.ctrl_msg_type = ctrl_msg_type
        self.host_name = host_name
        self.assigned_tunnel_id = assigned_tunnel_id
        self.assigned_session_id = assigned_session_id

    def get_avp_message_type(self,*args):
        attr_value = bitstruct.pack('u16',control_message_types[self.ctrl_msg_type][0])
        # Przykład stworzenia obiektu AVPFrame
        # Najpierw w tupli wymienia się poszczególne pola dla ramki AVP, a jako drugi argument przekazuje się pola dla ramki L2TP
        frame = AVPFrame((1,0,8,0,0,attr_value),*args)
        return frame
    def get_avp_protocol_version(self,*args):
        attr_value = bitstruct.pack('u8u8',1,0)
        frame = AVPFrame((1,0,8,0,2,attr_value),*args)
        return frame
    def get_avp_host_name(self,*args):
        attr_value_len = len(self.host_name)*8
        attr_value = bitstruct.pack('t'+str(attr_value_len),self.host_name)
        frame = AVPFrame((1,0,6+attr_value_len,0,7,attr_value),*args)
        return frame
    def get_avp_framing_cap(self,*args):
        attr_value = bitstruct.pack('p30b1b1',1,0)
        frame = AVPFrame((1,0,10,0,3,attr_value),*args)
        return frame
    def get_avp_assigned_tunnel_id(self,*args):
        attr_value = bitstruct.pack('u16',self.assigned_tunnel_id)
        frame = AVPFrame((1,0,8,0,9,attr_value),*args)
        return frame
    def get_avp_assigned_session_id(self,*args):
        attr_value = bitstruct.pack('u16',self.assigned_session_id)
        frame = AVPFrame((1,0,8,0,14,attr_value),*args)
        return frame
    def get_avp_call_serial_number(self,*args):
        attr_value = bitstruct.pack('u32',10)
        frame = AVPFrame((1,0,10,0,15,attr_value),*args)
        return frame
    def get_avp_connect_speed(self,*args):
        attr_value = bitstruct.pack('f32',100.0)
        frame = AVPFrame((1,0,10,0,24,attr_value),*args)
        return frame
    def get_avp_framing_type(self,*args):
        attr_value = bitstruct.pack('p30b1b1',1,0)
        frame = AVPFrame((1,0,10,0,3,attr_value),*args)
        return frame
    def get_avp_result_code(self,*args):
        attr_value = bitstruct.pack('u16',1)
        frame = AVPFrame((1,0,8,0,1,attr_value),*args)
        return frame
